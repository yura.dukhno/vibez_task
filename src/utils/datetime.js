import { format, getTime, isDate } from "date-fns";
//https://date-fns.org/v2.29.1/docs/format

export const getTimeFromDate = (date) => {
  let d = date;
  if (!isDate(date)) d = new Date(date);
  return getTime(d);
};

export const updateDateTime = (date, time) => {
  let d = new Date(date);
  let hours = time.split(":")[0];
  let minutes = time.split(":")[1];
  return d.setHours(hours, minutes);
};

export const normalizeDate = (date) => {
  return date ? format(new Date(date), "MM/dd/yyyy") : null;
};

export const normalizeDateTime = (date) => {
  return date ? format(new Date(date), "dd MMM, yyyy  |  HH:mm") : null;
};

export const timestampToTime = (timestamp) => {
  return timestamp ? format(new Date(timestamp), "HH:mm") : null;
};

export const calculateAge = (date) => {
  if (!date) return "";
  let birthday = new Date(date);
  let ageDifMs = Date.now() - birthday.getTime();
  let ageDate = new Date(ageDifMs); // miliseconds from epoch
  return Math.abs(ageDate.getUTCFullYear() - 1970);
};
