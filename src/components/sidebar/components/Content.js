// chakra imports
import { Box, Flex, Image, Stack, useColorModeValue } from "@chakra-ui/react";
//   Custom components
import SelectProject from "components/sidebar/components/Select";
import React from "react";
// Assets
import Logo from "assets/svg/NewOneLine.svg";

// FUNCTIONS

function SidebarContent(props) {
  let sidebarBackgroundColor = useColorModeValue("navy.800");
  const { routes } = props;
  // SIDEBAR
  return (
    <Flex
      direction="column"
      minH="100%"
      height="max-content"
      pt="25px"
      // borderRadius="30px"
      bgColor={sidebarBackgroundColor}
    >
      {/* <SelectProject /> */}
      <Stack direction="column" mb="auto" mt="8px" h={""}>
        <Box ps="20px" pe={{ md: "16px", "2xl": "1px" }}></Box>
      </Stack>

      <Flex w={"100%"} justifyContent="center" alignItems="center">
        <Box boxSize="100px">
          <Image src={Logo} />
        </Box>
      </Flex>
    </Flex>
  );
}

export default SidebarContent;
